#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<dirent.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>
#include<pwd.h>

char cur_dir[256];

int isType(char str[], char type[]){
	int i = 0, j = 0;
	while(str[i] != '\0'){
		if(type[j] == '\0') return i; //true
		if(str[i] == type[j]) j++;
		else j=0;
		i++;
	}
	return 0; //false
}



int main(){
	
	getcwd(cur_dir,sizeof(cur_dir)-1);
	
	pid_t new_id;
	
	//Make directory "darat"
	new_id = fork();
	if(new_id < 0) exit(EXIT_FAILURE);
	if(new_id == 0){
		char *argv[] = {"mkdir","-p","darat",NULL};
		execv("/usr/bin/mkdir",argv);
	}
	waitpid(new_id,NULL,0);
	puts("Successfully created darat folder");
	
	for(int i = 1; i <= 3; i++){
		printf("%d second has passed\n",i);
		sleep(1);
	}
	
	//make directory "air"
	new_id = fork();
	if(new_id < 0) exit(EXIT_FAILURE);
	if(new_id == 0){
		char *argv[] = {"mkdir","-p","air",NULL};
		execv("/usr/bin/mkdir",argv);
	}
	waitpid(new_id,NULL,0);
	puts("Successfully created air folder");
	//unzip "animal.zip"
	new_id = fork();
	if(new_id < 0) exit(EXIT_FAILURE);
	if(new_id == 0){
		DIR *temp = opendir("animal");
		if(temp){
			puts("Successfully deleted animal folder");
			int temp_id = fork();
			if(temp_id < 0) exit(EXIT_FAILURE);
			if(temp_id == 0){
				char *argv[] = {"rm","-r","animal",NULL};
				execv("/usr/bin/rm",argv);
			}
			waitpid(temp_id,NULL,0);
		}
		puts("Successfully unzipped animal.zip");
		char *argv[] = {"unzip","-q","animal.zip",NULL};
		execv("/usr/bin/unzip",argv);
	}
	waitpid(new_id,NULL,0);
	
	//memisahkan hewan darat dan air
	DIR *dir_scan;
	struct dirent *entry;
	if((dir_scan = opendir("animal")) == NULL) exit(EXIT_FAILURE);
	chdir("animal");
	while((entry = readdir(dir_scan)) != NULL){
		if(strcmp(".",entry->d_name) == 0 || 
			strcmp("..",entry->d_name) == 0) continue;
		int flag = 1;
		char *type;
		if(isType(entry->d_name, "darat")) type = "/darat";
		else if(isType(entry->d_name,"air")) type = "/air";
		else {
			flag = 0;
			remove(entry->d_name);	
		}
		if(flag){
			new_id = fork();
			if(new_id < 0) exit(EXIT_FAILURE);
			if(new_id == 0){
				char *dirtemp=
					(char*)malloc(strlen(cur_dir)
					+strlen(type)+1);
				dirtemp=cur_dir;
				strcat(dirtemp,type);
				char *argv[]={"mv",entry->d_name,dirtemp,
					NULL};
				execv("/usr/bin/mv",argv);
			}
			waitpid(new_id,NULL,0);
		}
	}
	chdir("..");
	puts("Successfully separated darat and air animals");
	
	//remove "bird" from darat
	if((dir_scan = opendir("darat")) == NULL) exit(EXIT_FAILURE);
	chdir("darat");
	while((entry = readdir(dir_scan)) != NULL){
		if(strcmp(".",entry->d_name) == 0 || 
			strcmp("..",entry->d_name) == 0) continue;
		if(isType(entry->d_name, "bird")) remove(entry->d_name);
	}
	chdir("..");
	puts("Successfully removed bird from darat");
	
	//memasukkan nama hewan air ke list.txt
	struct stat info;
	struct passwd *pw;
	FILE *list = fopen("list.txt","w");
	char temp[256];
	
	if((dir_scan = opendir("air")) == NULL) exit(EXIT_FAILURE);
	chdir("air");
	while((entry = readdir(dir_scan)) != NULL){
		if(strcmp(".",entry->d_name) == 0 || 
			strcmp("..",entry->d_name) == 0) continue;
		int i = isType(entry->d_name, "air");
		stat(entry->d_name,&info);
		pw = getpwuid(info.st_uid);
		fprintf(list,"%s_",pw->pw_name);
		if(info.st_mode & S_IRUSR) fprintf(list,"r");
		if(info.st_mode & S_IWUSR) fprintf(list,"w");
		if(info.st_mode & S_IXUSR) fprintf(list,"x");
		
		if(i - strlen("air") == 1) 
			fprintf(list,"%s\n",entry->d_name + i);
		else{
			memcpy(temp,entry->d_name,i-strlen("air"));
			temp[i-strlen("air")-1] = '\0';
			fprintf(list,"_%s%s\n",temp,entry->d_name+i);
			
		}
			
	}
	fclose(list);
	chdir("..");
	puts("Successfully inserted all animals from air to list.txt");

	return 1;
}
