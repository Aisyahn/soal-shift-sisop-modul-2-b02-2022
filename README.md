# Soal-shift-sisop-modul-2-B02-2022
# __Soal 1__
1. Mas Refadi adalah seorang wibu gemink.  Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu
	
	a. Mendownload file characters dan file weapons, lalu program akan mengekstrak kedua file tersebut. Membuat sebuah folder dengan nama "gacha_gacha" sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut.

	``` c
		/*** CREATE DIRECTORY ***/	
		char *mkdirArgs[][STRING_LENGTH] = {
			{"/usr/bin/mkdir", "mkdir", "-p", "./database/zip", NULL},
			{"/usr/bin/mkdir", "mkdir", "-p", "./gacha-gacha", NULL}
		};
		arrSize = SIZEOF(mkdirArgs);
		execPls(mkdirArgs, arrSize);

		/*** DOWNLOAD FILES ***/
		char *wgetArgs[][STRING_LENGTH] = {
			{"/usr/bin/wget", "wget", "-O", "database/zip/characters.zip", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", NULL},
			{"/usr/bin/wget", "wget", "-O", "database/zip/weapons.zip", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", NULL}
		};
		arrSize = SIZEOF(wgetArgs);
		execPls(wgetArgs, arrSize);
	```
	- saya membuat fungsi execPls yang gunanya mengeksekusi execv dengan loop dengan argumen yang dipass sebagai array.
	- dengan fungsi execPls saya mengeksekusi mkdir dan wget untuk menghasilkan hasil yang diminta

	b. Item characters dan item weapon akan selalu bergantian diambil datanya dari database. Maka untuk setiap kali jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters. Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut.  __Sehingga untuk setiap folder, akan terdapat 9 file (.txt) yang didalamnya berisi 10 hasil gacha.__ Dan karena ini simulasi gacha, maka hasil gacha di dalam file .txt adalah __ACAK/RANDOM__ dan setiap file (.txt) isi nya akan __BERBEDA__

	``` c
		if (jumGacha % 2 == 0){
			databaseDir = weaponDir;
			itemType = "weapon";
		} else {
			databaseDir = characterDir;
			itemType = "character";
		}

		if (jumGacha % 10 == 0) {
			sleep(1);
			txtName = generateName(jumGacha);
			printf("txt : %s\n", txtName);
		}

		pickRandomFiles(databaseDir, JSONProperties);
		JSONName = JSONProperties[0];
		JSONPath = JSONProperties[1];

		readJSON(JSONPath, name, rarity);
		writeFile(outDir, txtName, jumGacha, itemType, name, rarity, primogems);
	```
	- pemilihan kategori gacha akan dilakukan dengan if else, jika genap maka kategori = weapon, jika ganjil maka kategori = character. 
	- setiap txt dibatasi hanya 10 entry, jika lebih maka akan generate txt name baru dan menulis entry berikutnya disitu.
	- penulisan konten kedalam file .txt diberi batas sleep(1) untuk menghasilkan 1 detik dalam penulisan konten.

	c. Format penamaan setiap file (.txt) nya adalah {Hh:Mm:Ss}_gacha _{jumlah-gacha}, misal __04:44:12_gacha_120.txt__, dan format penamaan untuk setiap folder nya adalah total_gacha_{jumlah-gacha}, misal __total_gacha_270__. Dan untuk setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar __1 second__.

	``` c
		// in main
		if (jumGacha % 10 == 0) {
			sleep(1);
			txtName = generateName(jumGacha);
			printf("txt : %s\n", txtName);
		}

		if (jumGacha % 90 == 0) {
			outDir = generateDir(jumGacha);
			printf("dir : %s\n", outDir);
			
			char *createDirArgs[][STRING_LENGTH] = {
				{"/usr/bin/mkdir", "mkdir", "-p", outDir, NULL}
			};
			arrSize = SIZEOF(createDirArgs);
			execPls(createDirArgs, arrSize);
		}
		// in main

		char* generateName(int jumGacha){
			time_t current_time;
			struct tm * time_info;
			char *generated, timeString[9];
			size_t timeStringLen, gachaLen, jumGachaLen, extensionLen;

			time(&current_time);
			time_info = localtime(&current_time);
			strftime(timeString, sizeof(timeString), "%H:%M:%S", time_info);

			timeStringLen = strlen(timeString);
			gachaLen = strlen("gacha");
			jumGachaLen = floor(log10(abs(jumGacha))) + 1;
			extensionLen = strlen(".txt");

			generated = malloc(sizeof(char) * (timeStringLen + gachaLen + jumGachaLen, extensionLen + 1));
			sprintf(generated, "%s_gacha_%d.txt", timeString, jumGacha);

			return generated;
		}

		char* generateDir(int jumGacha) {
			char *dir = malloc(sizeof(char) * 256);
			sprintf(dir, "./gacha-gacha/total_gacha_%d/", jumGacha);

			return dir;
		}		
	```
	- setiap 10 kali entry akan dibuat nama txt baru sehingga entry berikutnya akan write di file txt baru.
	- setiap 90 kali entry akan dibuat folder baru menggunakan execPls mkdir.
	

	d. Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan primogems. Satu kali gacha item akan menghabiskan primogems sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya terlihat banyak, maka pada program, primogems di awal di-define sebanyak __79000__ primogems. Setiap kali gacha, ada __2 properties__ yang akan diambil dari database, yaitu __name__ dan __rarity__. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha {jumlah-gacha}_ [tipe -item]_ {rarity}_{name}_{sisa-primogems}. Program akan selalu melakukan gacha hingga primogems habis.
	__Contoh : 157_characters_5_Albedo_5388__

	``` c
		int writeFile(char *fileDir, char *fileName, int jumGacha, char *itemType, char *name, char *rarity, int sisaPrimogems){
			FILE *fp;
			char *filePath, *input;
			size_t fileDirLen, fileNameLen, jumGachaLen, tipeItemLen, rarityLen, nameLen, sisaPrimogemsLen;

			fileDirLen = strlen(fileDir);
			fileNameLen = strlen(fileName);

			filePath = malloc(sizeof(char) * (fileDirLen + fileNameLen + 1));
			strcpy(filePath, fileDir);
			strcat(filePath, fileName);

			tipeItemLen = strlen(itemType);
			rarityLen = strlen(rarity);
			nameLen = strlen(name);

			if(jumGacha == 0) jumGachaLen = 1;
			else jumGachaLen = floor(log10(abs(jumGacha))) + 1;

			if(sisaPrimogemsLen == 0) sisaPrimogemsLen = 1;
			else sisaPrimogemsLen = floor(log10(abs(sisaPrimogems))) + 1;

			input = malloc(sizeof(char) * (tipeItemLen + rarityLen + nameLen + jumGachaLen + sisaPrimogemsLen + 4 + 1 + 1));
			sprintf(input, "%d_%s_%s_%s_%d\n", jumGacha, itemType, rarity, name, sisaPrimogems);

			fp = fopen(filePath, "a");
			fputs(input, fp);
			fclose(fp);

			return EXIT_SUCCESS;
		}
	```
	- sesuai dengan nama fungsinya, fungsi ini akan mengambil parameter yang ada kemudian menuliskannnya pada file yang diminta dengan format yang telah ditentukan.
	
	e. Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain bengshin impek, yaitu pada __30 Maret jam 04:44__.  Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka __3 jam__ setelah anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)

	``` c
		int checkTime(char *threshold){
			double diff;
			struct tm *tmNow, tmThreshold;
			time_t timeNow, timeThreshold;

			strptime(threshold, "%F %T", &tmThreshold);

			timeNow = time(0);
			tmNow = localtime(&timeNow);
			timeNow = mktime(tmNow);

			timeThreshold = mktime(&tmThreshold);

			diff = difftime(timeThreshold, timeNow);
			if(diff >= 0) return EXIT_SUCCESS;
			else return EXIT_FAILURE;
		}
	```
	- fungsi ini akan membaca time threshold kemudian memmbandingkannya dengan waktu sekarang, jika sudah lewat treshold maka return 1, else return -1

<hr>

# __Soal 2__

2. Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.

    a. Mengextract zip yang diberikan ke dalam folder "/home/[user]/shift2/drakor". Lalu Menghapus folder-folder yang tidak dibutuhkan.

    ``` c

	...

	if (child_id == 0){
		//A. membuat folder dan mengextract zip
        char *argv[] = {"mkdir", "-p", path, NULL};
        execv("/bin/mkdir", argv);
	}
	else {
		pid_t child_id2;
		child_id2 = fork();
		
		if(child_id2 == 0){
			chdir("drakor");
			char *argv[] = {"unzip", "drakor.zip", "-d", "/home/aisy/shift2/drakor", "*.png",NULL};
			execv("/bin/unzip", argv);
    	}

	...

	```

	- `mkdir` digunakan untuk membuat folder baru yaitu folder 'drakor'
	- 'unzip' digunakan untuk mengextract folder drakor.zip

	b. Membuat folder untuk setiap jenis drama  korea yang ada di dalam zip. 
	__Contoh:__ Jenis drama korea romance akan disimpan dalam "/drakor/romance", dst.

    ``` c

	...

	void create_folder(struct drakor *ptr, int index, char path[]){
		char tmp[100];
		strcpy(tmp, path);
		strcat(tmp, "/");
		strcat(tmp, (ptr+index)->genre);

		pid_t child_id1_id;
		child_id1_id = fork();
		int status;

		if(child_id1_id < 0){
			exit(EXIT_FAILURE);
		}
		if(child_id1_id == 0){
			char *argv[] = {"mkdir", "-p", tmp, NULL};
			execv("/bin/mkdir", argv);
		} else {
			while ((wait(&status))>0);
			return;
		}
	}

	...

	```
	- Membuat folder berdasarkan file .png yang sudah dibaca sebelumnya (pada poin d)
	- `strcpy` digunakan untuk mengcopy 'path' ke variabel 'tmp'
	- `strcat` digunakan untuk menambahkan string ke dalam variabel 'tmp'

    c. Memindahkan poster ke folder sesuai dengan kategorinya dan di rename dengan nama.
	__Contoh:__ "/drakor/romance/start-up.png".

    ``` c

	...

	void move_genre(struct drakor *ptr, int index, char path[]){
		char from[100];
		strcpy(from, path);
		strcat(from, "/");
		strcat(from, (ptr+index)->name_file);
		
		char destination[100];
		strcpy(destination, path);
		strcat(destination, (ptr+index)->genre);
		strcat(destination, "/");
		strcat(destination, (ptr+index)->title);
		strcat(destination, ".png");

		pid_t child_id2_id;
		child_id2_id = fork();
		int status;
		
		if(child_id2_id < 0){
			exit(EXIT_FAILURE);
		}
		if(child_id2_id == 0){
			char *argv[] = {"cp", from, destination, NULL};
			execv("/usr/bin/cp", argv);
		} else {
			while ((wait(&status)) > 0);
			return;
		}
	}

	...

	```

	- memindahkan poster berdasarkan folder yang sudah dibuat sebelumnya
	- `cp` digunakan untuk mengcopy file 


    d. Dalam satu foto bisa terdapat lebih dari satu poster maka foto harus dipindah ke masing-masing kategori yang sesuai.
	__Contoh:__ foto dengan nama "start-up;2020;romance_the-k2:2016:action.png" dipindah ke folder "/drakor/romance/start-up.png" dan "drakor/action/the-k2.png".

	```c

	...
	
	while ((dp = readdir(dir)) != NULL){
		if(strcmp(dp->d_name, ".") !=0 && strcmp(dp->d_name, "..") != 0){
			int flag1 = 0;
			strcpy(data[flag].name_file, dp->d_name);

			if(strchr(data[flag].name_file, '_')!= 0){
				flag1++;
			}	
			char *tok = strtok(dp->d_name, "_;.");
			for(int i=0; tok!=NULL; i++){
				if(i==0){
					strcpy(data[flag].title, tok);
				}else if (i==1){
					data[flag].year=atoi(tok);
				}else if (i==2){
					strcpy(data[flag].genre, tok);
				}else if (i==3){
					flag++;
							
					if(flag1!=0){
						strcpy(data[flag].name_file, data[flag-1].name_file);
						strcpy(data[flag].title, tok);
						flag1=0;
						}
						i=0;
				}
				tok = strtok (NULL, "_;.");
			}	
		}	
	}	

	...			

	```
	- untuk poin d sendiri dapat menggunakan `strtok` untuk memisahkkan string, selanjutnya memindahkan folder dengan fungsi `move_genre`
	  
    e. Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea yang ada dalam folder tersebut, sorting list berdasarkan tahun rilis (Ascending).

	```c
	void list_genre(struct drakor *ptr, int flag){
	struct drakor list[100];
	int index_list = flag;
	
	for(int i=0; i<flag; i++){
		strcpy(list[i].genre, (ptr+i)->genre);
	}
	
	//menghapus duplicate genre
	for(int i=0; i<index_list; i++){
		for(int j=i+1; j<index_list; j++){
			if(strcmp(list[i].genre, list[j].genre)==0){
				for(int k=j; k<index_list; k++){
					strcpy(list[k].genre, list[k+1].genre);
				}
				j--;
				index_list--;
			}	
		}
	}
	
	...

	```
	- sebelum membuat file .txt dilakukan pembuatan fungsi `list_genre` yang digunakan untuk menghapus duplicate genre, menglist genre, dan menampilkan file .txt 

    ``` c

	...

	for(int i=0; i<index_list; i++){
		struct drakor tmp[100];
		int index_tmp=0;
		
		for(int j=0; j<flag; j++){
			if(strcmp(list[i].genre, (ptr+j)->genre)==0){
				strcpy(tmp[index_tmp].title, (ptr+j)->title);
				tmp[index_tmp].year=(ptr+j)->year;
				index_tmp++;
			}
		}
		
		for(int j=0; j<index_tmp-1; j++){
			int min_id=j;
			
			for(int k=j+1; k<index_tmp; k++){
				if(tmp[k].year<tmp[min_id].year){
					min_id=k;
					struct drakor tmp2;
 					strcpy(tmp2.title,tmp[min_id].title);
						tmp2.year=tmp[min_id].year;
					strcpy(tmp[min_id].title, tmp[j].title);
						tmp[min_id].year=tmp[j].year;
					strcpy(tmp[j].title, tmp2.title);
						tmp[j].year=tmp2.year;
				}		
			}
		}

		char text_path[100];
		strcpy(text_path, "/home/aisy/shift2/drakor/");
		strcat(text_path, list[i].genre);
		strcat(text_path, "/data.txt");

		pid_t child_id_id;
		child_id_id = fork();
		
		int status;
		if(child_id_id <0){
			exit(EXIT_FAILURE);
		}
		if(child_id_id == 0){
			char *argv[] = {"touch", text_path, NULL};
			execv("/usr/bin/touch", argv);
		} 
		else {
			while((wait(&status)) > 0);
		}

		char isi_text[1000];
		strcpy(isi_text, "kategori : ");
		strcat(isi_text, list[i].genre);
		
		for(int i=0; i<index_tmp; i++){
			strcat(isi_text, "\n\n");
			strcat(isi_text, "nama : ");
			strcat(isi_text, tmp[i].title);
			strcat(isi_text, "\n");
			strcat(isi_text, "rilis : ");
			
			char movie_year[10];
			sprintf(movie_year, "%d", (tmp[i].year));
			strcat(isi_text, movie_year);
		}
		
		FILE *fptr=fopen(text_path, "a");
		fputs(isi_text, fptr);
		fclose(fptr);
	}
	
	...
	
	```
	- menentukan destination path file .txt menggunakan `text_path`
	- menuliskan kategori, nama, lalu tahun rilis yang mana tahun rilis diurutkan secara ascending ke dalam file `isi_text`


<hr>

# __Soal 3__
3. Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang

    a. Membuat 2 directory di "/home/[USER]/modul2/" dengan nama "darat" lalu 3 detik kemudian membuat directory ke 2 dengan nama "air".
    
    ``` c
	//Make directory "darat"
	new_id = fork();
	if(new_id < 0) exit(EXIT_FAILURE);
	if(new_id == 0){
		char *argv[] = {"mkdir","-p","darat",NULL};
		execv("/usr/bin/mkdir",argv);
	}
	waitpid(new_id,NULL,0);
	puts("Successfully created darat folder");
	
	for(int i = 1; i <= 3; i++){
		printf("%d second has passed\n",i);
		sleep(1);
	}
	
	//make directory "air"
	new_id = fork();
	if(new_id < 0) exit(EXIT_FAILURE);
	if(new_id == 0){
		char *argv[] = {"mkdir","-p","air",NULL};
		execv("/usr/bin/mkdir",argv);
	}
	waitpid(new_id,NULL,0);
	puts("Successfully created air folder");
	}
	```

    b. Melakukan extract "animal.zip" di "/home/[USER]/modul2/".

     ``` c
	//unzip "animal.zip"
	new_id = fork();
	if(new_id < 0) exit(EXIT_FAILURE);
	if(new_id == 0){
		DIR *temp = opendir("animal");
		if(temp){
			puts("Successfully deleted animal folder");
			int temp_id = fork();
			if(temp_id < 0) exit(EXIT_FAILURE);
			if(temp_id == 0){
				char *argv[] = {"rm","-r","animal",NULL};
				execv("/usr/bin/rm",argv);
			}
			waitpid(temp_id,NULL,0);
		}
		puts("Successfully unzipped animal.zip");
		char *argv[] = {"unzip","-q","animal.zip",NULL};
		execv("/usr/bin/unzip",argv);
	}
	waitpid(new_id,NULL,0);
	```

    c. Hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Hewan darat "/home/[USER]/modul2/darat" dan hewan air "/home/[USER]/modul2/air". Rentang pembuatan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan tidak ada keterangan air atau darat harus dihapus.

    ``` c
	DIR *dir_scan;
	struct dirent *entry;
	if((dir_scan = opendir("animal")) == NULL) exit(EXIT_FAILURE);
	chdir("animal");
	while((entry = readdir(dir_scan)) != NULL){
		if(strcmp(".",entry->d_name) == 0 || 
			strcmp("..",entry->d_name) == 0) continue;
		int flag = 1;
		char *type;
		if(isType(entry->d_name, "darat")) type = "/darat";
		else if(isType(entry->d_name,"air")) type = "/air";
		else {
			flag = 0;
			remove(entry->d_name);	
		}
		if(flag){
			new_id = fork();
			if(new_id < 0) exit(EXIT_FAILURE);
			if(new_id == 0){
				char *dirtemp=
					(char*)malloc(strlen(cur_dir)
					+strlen(type)+1);
				dirtemp=cur_dir;
				strcat(dirtemp,type);
				char *argv[]={"mv",entry->d_name,dirtemp,
					NULL};
				execv("/usr/bin/mv",argv);
			}
			waitpid(new_id,NULL,0);
		}
	}
	chdir("..");
	puts("Successfully separated darat and air animals");
	```

    d. Menghapus semua burung yang ada di directory "/home/[USER]/modul2/darat". Hewan burung ditandai dengan adanya "bird" pada nama file.

	 ``` c
	//remove "bird" from darat
	if((dir_scan = opendir("darat")) == NULL) exit(EXIT_FAILURE);
	chdir("darat");
	while((entry = readdir(dir_scan)) != NULL){
		if(strcmp(".",entry->d_name) == 0 || 
			strcmp("..",entry->d_name) == 0) continue;
		if(isType(entry->d_name, "bird")) remove(entry->d_name);
	}
	chdir("..");
	puts("Successfully removed bird from darat");
	```
	e. Mmebuat file list.txt di folder "/home/[USER]/modul2/air" dan membuat list nama semua hewan yang ada di directory "/home/[USER]/modul2/air" ke list.txt dengan format UID_[UID file permission]_Nama File.[jpg/png].
	__Contoh:__ conan_rwx_hewan.png

	
	 ``` c
	//memasukkan nama hewan air ke list.txt
	struct stat info;
	struct passwd *pw;
	FILE *list = fopen("list.txt","w");
	char temp[256];
	
	if((dir_scan = opendir("air")) == NULL) exit(EXIT_FAILURE);
	chdir("air");
	while((entry = readdir(dir_scan)) != NULL){
		if(strcmp(".",entry->d_name) == 0 || 
			strcmp("..",entry->d_name) == 0) continue;
		int i = isType(entry->d_name, "air");
		stat(entry->d_name,&info);
		pw = getpwuid(info.st_uid);
		fprintf(list,"%s_",pw->pw_name);
		if(info.st_mode & S_IRUSR) fprintf(list,"r");
		if(info.st_mode & S_IWUSR) fprintf(list,"w");
		if(info.st_mode & S_IXUSR) fprintf(list,"x");
		
		if(i - strlen("air") == 1) 
			fprintf(list,"%s\n",entry->d_name + i);
		else{
			memcpy(temp,entry->d_name,i-strlen("air"));
			temp[i-strlen("air")-1] = '\0';
			fprintf(list,"_%s%s\n",temp,entry->d_name+i);
			
		}
			
	}
	fclose(list);
	chdir("..");
	puts("Successfully inserted all animals from air to list.txt");
	```

## Dokumentasi pengerjaan dan kendala
### Soal 1
![dokum-soal-1](/uploads/c71a7b0a76598cd9708ded8b4ab1df15/image.png)
![dokum-soal-1](/uploads/3abcf82247f60117301795a25de9d850/image.png)
![dokum-soal-1](/uploads/66d4f4b96aa3b93c76613842180c7d64/image.png)

Berikut adalah beberapa kendala yang saya jumpai selama mengerjakan soal1 :
- salah baca soal, kirain gaboleh exec, syscall, fork
- salah link untuk wget/curl
- 403 forbidden request untuk wget/curl
- seg error pas main ama pointer/buffer
- pas di daemonin zip gak ke download
- kadang download suka corrupt
- lama pas implementasi fungsi2 biar enak dibaca


## Dokumentasi pengerjaan dan kendala
### Soal 2
![soal2](/uploads/42365b46af3987e1f8ee7405bcf55477/soal2.jpg)

Berikut adalah beberapa kendala dalam soal2:
- kurang tau cara membaca file untuk dijadikan nama folder
- sulit untuk sorting tahun di file txt

## Dokumentasi pengerjaan dan kendala
### Soal 3
![soal3](/uploads/0b5b398bed2152aedb667690f65c2782/soal3.png)

![soal3_1](/uploads/5e2402d3ee4c537eaea51bd3cd596325/soal3_1.png)

![soal3_2](/uploads/3f61a37b9348ac9db5321b3a19f53ea6/soal3_2.png)

![soal3_3](/uploads/1d826528aa56f98a93508ce2d6e2ffb7/soal3_3.png)


Berikut adalah beberapa kendala dalam soal3:
- Tidak bisa run C, sehingga harus setting dulu dan malah jadi error VM linuxnya sehingga buat baru
- tidak berhasil nge-run execv nya 
- ternyata karena kelebihian "/" saat execv
- ada error logika saat membuat list.txt sehingga nama hewannya aneh, ternyata harus ditambah '\0' karakter diindex terakhir string saat ini




	
