/*
INIT STUFF
- Download files
- Extract files
- Jadiin file sebagai database
- Buat n cd to folder "gacha gacha"

GACHA STUFF
- Currency 		: Primogem
- Init value	: 79000
- 1x gacha		: 160 primogems
- Properties
	> name
	> rarity

APP STUFF
- Gacha ampe primogem abis
	> ganjil	: character
	> genap 	: weapon

OUTPUT STUFF
- setiap # gacha
	> mod 10	: bikin file txt baru
	> mod 90 	: bikin folder n txt baru
- format 
	> folder	: total_gacha_{jumlah-gacha}
	> txt 		: {Hh:Mm:Ss}_gacha_{jumlah-gacha}
	> isi txt	: {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}

OPERATIONAL STUFF
- 1 script, daemon
- gacha starts at 30 Maret 04:44
- 3 jam setelah gacha, folder di .zip
	> nama	: not_safe_for_wibu
	> pw	: satuduatiga
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <dirent.h>
#include <unistd.h>
#include <time.h>
#include <syslog.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>

#include <json-c/json.h>

#define INIT_PRIMOGEMS 79000
#define HARGA_GACHA 160
#define STRING_LENGTH 200
#define SIZEOF(a) sizeof(a)/sizeof(*a)

int pipeExecOut(char *cmdArgs1[], char *cmdArgs2[]);
int countFile(char *dir);
int pickRandomFiles(char *dir, char *fileProperties[2]);
int readJSON(char *filePath, char *name, char *rarity);
int writeFile(char *fileDir, char *fileName, int jumGacha, char *itemType, char *name, char *rarity, int sisaPrimogems);
int execPls(char *cmdArgs[][STRING_LENGTH], size_t arr_size);
int checkTime(char *threshold);

char* removeExt(char *file);
char* generateName(int jumGacha);
char* generateDir(int jumGacha);

static void daemonize();

int main(int argc, char *argv[]) {	
	char *timeToGacha = "2022-03-30 04:44";
	char *timeToZip = "2022-03-30 07:44";

	daemonize();

	while (1) {
		time_t now, execute, zip;
		struct tm *tstr;

		size_t arrSize;
		srand(time(NULL));
		/*** CREATE DIRECTORY ***/	
		char *mkdirArgs[][STRING_LENGTH] = {
			{"/usr/bin/mkdir", "mkdir", "-p", "./database/zip", NULL},
			{"/usr/bin/mkdir", "mkdir", "-p", "./gacha-gacha", NULL}
		};
		arrSize = SIZEOF(mkdirArgs);
		execPls(mkdirArgs, arrSize);

		/*** DOWNLOAD FILES ***/
		char *wgetArgs[][STRING_LENGTH] = {
			{"/usr/bin/wget", "wget", "-qO", "database/zip/characters.zip", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", NULL},
			{"/usr/bin/wget", "wget", "-qO", "database/zip/weapons.zip", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", NULL}
		};
		arrSize = SIZEOF(wgetArgs);
		execPls(wgetArgs, arrSize);

		/*** UNZIP FILES ***/
		char *unzipArgs[][STRING_LENGTH] = {
			{"/usr/bin/unzip", "unzip", "-qo", "./database/zip/characters.zip", "-d", "./database/", NULL},
			{"/usr/bin/unzip", "unzip", "-qo", "./database/zip/weapons.zip", "-d", "./database/", NULL}
		};
		arrSize = SIZEOF(unzipArgs);
		execPls(unzipArgs, arrSize);

		/*** MONEY GOES BRRR ***/
		char *characterDir = "./database/characters/";
		char *weaponDir = "./database/weapons/";
		char *databaseDir, *outDir;
		char *JSONName, *JSONPath, *JSONProperties[2];
		char *txtName;

		char *itemType, name[0x100], rarity[0x100];
		int primogems, jumGacha;

		primogems = INIT_PRIMOGEMS;
		jumGacha = 0;
		
		if(checkTime(timeToGacha)){
			while(primogems > HARGA_GACHA) {
				primogems -= HARGA_GACHA;
				
				if (jumGacha % 2 == 0){
					databaseDir = weaponDir;
					itemType = "weapon";
				} else {
					databaseDir = characterDir;
					itemType = "character";
				}

				if (jumGacha % 10 == 0) {
					sleep(1);
					txtName = generateName(jumGacha);
					printf("txt : %s\n", txtName);
				} 
				
				if (jumGacha % 90 == 0) {
					outDir = generateDir(jumGacha);
					printf("dir : %s\n", outDir);
					
					char *createDirArgs[][STRING_LENGTH] = {
						{"/usr/bin/mkdir", "mkdir", "-p", outDir, NULL}
					};
					arrSize = SIZEOF(createDirArgs);
					execPls(createDirArgs, arrSize);
				}
				
				pickRandomFiles(databaseDir, JSONProperties);
				JSONName = JSONProperties[0];
				JSONPath = JSONProperties[1];

				readJSON(JSONPath, name, rarity);
				writeFile(outDir, txtName, jumGacha, itemType, name, rarity, primogems);

				jumGacha += 1;
			}
		}

		if(checkTime(timeToZip)){
			char *zipArgs[][STRING_LENGTH] = {
				{"/usr/bin/zip", "zip", "-r", "--password", "satuduatiga", "not_safe_for_wibu", "gacha-gacha"}
			};
			arrSize = SIZEOF(zipArgs);
			execPls(zipArgs, arrSize);
			char *rmArgs[][STRING_LENGTH] = {
				{"/usr/bin/rm", "rm", "-rd", "./gacha-gacha"}
			};
			arrSize = SIZEOF(rmArgs);
			execPls(rmArgs, arrSize);

			sleep(30);
			break;
		}
	}

	syslog (LOG_NOTICE, "soal1 daemon terminated.");
    closelog();

	return EXIT_SUCCESS;
}

static void daemonize(){
	pid_t pid;

	pid = fork();
	if (pid < 0) _Exit(EXIT_FAILURE);
	if (pid > 0) _Exit(EXIT_SUCCESS);
	if (setsid() < 0) _Exit(EXIT_FAILURE);

	pid = fork();
	if (pid < 0) _Exit(EXIT_FAILURE);
	if (pid > 0) _Exit(EXIT_SUCCESS);
	umask(0);

	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
	openlog("soal1 daemon", LOG_PID, LOG_DAEMON);
}

int checkTime(char *threshold){
	double diff;
	struct tm *tmNow, tmThreshold;
	time_t timeNow, timeThreshold;

	strptime(threshold, "%F %T", &tmThreshold);

	timeNow = time(0);
	tmNow = localtime(&timeNow);
	timeNow = mktime(tmNow);

	timeThreshold = mktime(&tmThreshold);

	diff = difftime(timeThreshold, timeNow);
	if(diff >= 0) return EXIT_SUCCESS;
	else return EXIT_FAILURE;
}

int execPls(char *cmdArgs[][STRING_LENGTH], size_t arrSize){
	pid_t child_id;

	for (int i = 0; i < arrSize; i++) {
		child_id = fork();
		if (child_id < 0) _Exit(EXIT_FAILURE);
		if (child_id == 0) execv(cmdArgs[i][0], &cmdArgs[i][1]);
		while(wait(NULL) > 0) ;
	}
	return EXIT_SUCCESS;
}

char* generateDir(int jumGacha) {
	char *dir = malloc(sizeof(char) * 256);
	sprintf(dir, "./gacha-gacha/total_gacha_%d/", jumGacha);

	return dir;
}

int writeFile(char *fileDir, char *fileName, int jumGacha, char *itemType, char *name, char *rarity, int sisaPrimogems){
	FILE *fp;
	char *filePath, *input;
	size_t fileDirLen, fileNameLen, jumGachaLen, tipeItemLen, rarityLen, nameLen, sisaPrimogemsLen;

	fileDirLen = strlen(fileDir);
	fileNameLen = strlen(fileName);

	filePath = malloc(sizeof(char) * (fileDirLen + fileNameLen + 1));
	strcpy(filePath, fileDir);
	strcat(filePath, fileName);

	tipeItemLen = strlen(itemType);
	rarityLen = strlen(rarity);
	nameLen = strlen(name);

	if(jumGacha == 0) jumGachaLen = 1;
	else jumGachaLen = floor(log10(abs(jumGacha))) + 1;

	if(sisaPrimogemsLen == 0) sisaPrimogemsLen = 1;
	else sisaPrimogemsLen = floor(log10(abs(sisaPrimogems))) + 1;

	input = malloc(sizeof(char) * (tipeItemLen + rarityLen + nameLen + jumGachaLen + sisaPrimogemsLen + 4 + 1 + 1));
	sprintf(input, "%d_%s_%s_%s_%d\n", jumGacha, itemType, rarity, name, sisaPrimogems);

	fp = fopen(filePath, "a");
	fputs(input, fp);
	fclose(fp);

	return EXIT_SUCCESS;
}

char* generateName(int jumGacha){
	time_t current_time;
	struct tm * time_info;
	char *generated, timeString[9];
	size_t timeStringLen, gachaLen, jumGachaLen, extensionLen;

	time(&current_time);
	time_info = localtime(&current_time);
	strftime(timeString, sizeof(timeString), "%H:%M:%S", time_info);

	timeStringLen = strlen(timeString);
	gachaLen = strlen("gacha");
	jumGachaLen = floor(log10(abs(jumGacha))) + 1;
	extensionLen = strlen(".txt");
	

	generated = malloc(sizeof(char) * (timeStringLen + gachaLen + jumGachaLen, extensionLen + 1));
	sprintf(generated, "%s_gacha_%d.txt", timeString, jumGacha);

	return generated;
}

char* removeExt(char *fileName) {
    char *result;
    char *lastExt;
    if (fileName == NULL) return NULL;
    if ((result = malloc (strlen (fileName) + 1)) == NULL) return NULL;
    strcpy (result, fileName);
    lastExt = strrchr (result, '.');
    if (lastExt != NULL)
        *lastExt = '\0';
    return result;
}

int readJSON(char *filePath, char *name, char *rarity) {
	FILE *fp;
	char buffer[0x1000];
	struct json_object *parsedJSON;
	struct json_object *nameJSON;
	struct json_object *rarityJSON;

	fp = fopen(filePath, "r");
	fread(buffer, 0x1000, 1, fp);
	fclose(fp);

	parsedJSON = json_tokener_parse(buffer);
	nameJSON = json_object_object_get(parsedJSON, "name");
	rarityJSON = json_object_object_get(parsedJSON, "rarity");

	strcpy(name, strdup(json_object_get_string(nameJSON)));
	strcpy(rarity, strdup(json_object_get_string(rarityJSON)));
	
	return EXIT_SUCCESS;
}

int pickRandomFiles(char *dir, char *fileProperties[2]){
	struct dirent *entry;
    DIR *dp;
	char* filePath;
	char* fileName;
	size_t pathLen, nameLen;
	
	int fileCount = countFile(dir) - 2;
	int randomNum = rand() % fileCount;
	
	int count = 0;
    dp = opendir(dir);
    if (dp != NULL) 
		while ((entry = readdir(dp))){
			if(strcmp(".", entry -> d_name) == 0 || strcmp("..", entry -> d_name) == 0) {
				entry = readdir(dp);
				continue;
			}

			if(count == randomNum) break;
			count++;
		}
	fileName = entry -> d_name;
    closedir(dp);

	pathLen = strlen(dir);
	nameLen = strlen(fileName);
	filePath = malloc(sizeof(char) * (pathLen + nameLen + 1));

	strcpy(filePath, dir);
	strcat(filePath, fileName);

	fileName = removeExt(fileName);

	fileProperties[0] = fileName;
	fileProperties[1] = filePath;

	return EXIT_SUCCESS;
}

int countFile(char *dir){
	char *lsArgs[] = {"/usr/bin/ls", "ls", dir, NULL};
	char *wcArgs[] = {"/usr/bin/wc", "wc", "-l", NULL};
	return pipeExecOut(lsArgs, wcArgs);
}

int pipeExecOut(char *cmdArgs1[], char *cmdArgs2[]){
	size_t first_id;
	size_t second_id;
	char res[100];
	int result = -1;
	int pipefd1[2];
	int pipefd2[2];

	pipe(pipefd1);
	first_id = fork();

	if (first_id < 0) _Exit(EXIT_FAILURE);
	if (first_id == 0) {
		dup2(pipefd1[1], STDOUT_FILENO);
		close(pipefd1[0]);
		execv(cmdArgs1[0], &cmdArgs1[1]);
	}
	else {
		dup2(pipefd1[0], STDIN_FILENO);
		close(pipefd1[1]);

		pipe(pipefd2);
		second_id = fork();
		if (second_id < 0) _Exit(EXIT_FAILURE);
		if (second_id == 0) {
			dup2(pipefd2[1], STDOUT_FILENO);
            close(pipefd2[0]); 
			execv(cmdArgs2[0], &cmdArgs2[1]);
		} else {
			while(wait(NULL) > 0);
			read(pipefd2[0], res, sizeof(res));
			result = atoi(res);
		}
	}

	return result;
}

/*
Database item characters 	: https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view
Database item weapons 		: https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view
Dokumentasi parsing json	: https://progur.com/2018/12/how-to-parse-json-in-c.html
*/